package constants

var PersonNotFound = "person cannot be empty"
var PersonAlreadyExists = "person already exists"
var PersonNotPresent = "person not found"
var NameCannotBeEmpty = "name cannot be empty"
