package main

import (
	"context"
	"log"

	pb "addressbook-grpc/proto" // Import the generated protobuf code

	"google.golang.org/grpc"
)

func main() {
	// Set up a connection to the server
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to dial: %v", err)
	}
	defer conn.Close()

	client := pb.NewAddressBookServiceClient(conn) // Create a new client instance

	// add a person
	addReq := &pb.AddPersonRequest{
		Person: &pb.Person{
			Name:    "Alice",
			Phone:   "555-1234",
			Address: "123 Main St",
		},
	}
	addRes, err := client.AddPerson(context.Background(), addReq)
	if err != nil {
		log.Fatalf("AddPerson failed: %v", err)
	}
	log.Printf("AddPerson response: %v", addRes)

	// get a person
	getReq := &pb.GetPersonRequest{
		Name: "Alice",
	}
	getRes, err := client.GetPerson(context.Background(), getReq)
	if err != nil {
		log.Fatalf("GetPerson failed: %v", err)
	}
	log.Printf("GetPerson response: %v", getRes.Person)

	// update a person
	updateReq := &pb.UpdatePersonRequest{
		Person: &pb.Person{
			Name:    "Alice",
			Phone:   "555-5678",
			Address: "456 Oak St",
		},
	}
	updateRes, err := client.UpdatePerson(context.Background(), updateReq)
	if err != nil {
		log.Fatalf("UpdatePerson failed: %v", err)
	}
	log.Printf("UpdatePerson response: %v", updateRes)

	// delete a person
	deleteReq := &pb.DeletePersonRequest{
		Name: "Alice",
	}
	deleteRes, err := client.DeletePerson(context.Background(), deleteReq)
	if err != nil {
		log.Fatalf("DeletePerson failed: %v", err)
	}
	log.Printf("DeletePerson response: %v", deleteRes)
}
