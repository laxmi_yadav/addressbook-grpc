package main

import (
	"context"
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"

	"addressbook-grpc/constants"
	pb "addressbook-grpc/proto" // Import the generated protobuf code
)

// Define a struct to implement the AddressBookServiceServer interface
type server struct {
	pb.UnimplementedAddressBookServiceServer // Make sure to embed the UnimplementedAddressBookServiceServer to ensure the server satisfies the interface

	persons map[string]*pb.Person // Use a map to store the Person objects
}

// Implement the AddPerson gRPC method

func (s *server) AddPerson(ctx context.Context, req *pb.AddPersonRequest) (*pb.AddPersonResponse, error) {
	person := req.GetPerson()
	if person == nil {
		return nil, fmt.Errorf(constants.PersonNotFound) //Error "person cannot be empty"
	}
	if _, ok := s.persons[person.Name]; ok {
		return nil, fmt.Errorf(constants.PersonAlreadyExists) //Error "person already exists"
	}
	s.persons[person.Name] = person // Add the Person object to the map
	return &pb.AddPersonResponse{Message: "Person added successfully"}, nil
}

// Implement the GetPerson gRPC method

func (s *server) GetPerson(ctx context.Context, req *pb.GetPersonRequest) (*pb.GetPersonResponse, error) {
	name := req.GetName()
	if name == "" {
		return nil, fmt.Errorf(constants.NameCannotBeEmpty) //error "name cannot be empty"
	}
	person, ok := s.persons[name]
	if !ok {
		return nil, fmt.Errorf(constants.PersonNotPresent) //error "person not found"
	}
	return &pb.GetPersonResponse{Person: person}, nil // Return the Person object
}

// Implement the UpdatePerson gRPC method
func (s *server) UpdatePerson(ctx context.Context, req *pb.UpdatePersonRequest) (*pb.UpdatePersonResponse, error) {
	person := req.GetPerson()
	if person == nil {
		return nil, fmt.Errorf(constants.PersonNotFound) //Error "person cannot be empty"
	}
	if _, ok := s.persons[person.Name]; !ok {
		return nil, fmt.Errorf(constants.PersonNotPresent) //error "person not found"
	}
	s.persons[person.Name] = person // Update the Person object in the map
	return &pb.UpdatePersonResponse{Message: "Person updated successfully"}, nil
}

// Implement the DeletePerson gRPC method
func (s *server) DeletePerson(ctx context.Context, req *pb.DeletePersonRequest) (*pb.DeletePersonResponse, error) {
	name := req.GetName()
	if name == "" {
		return nil, fmt.Errorf(constants.NameCannotBeEmpty) //error "name cannot be empty"
	}
	if _, ok := s.persons[name]; !ok {
		return nil, fmt.Errorf(constants.PersonNotPresent) //error "person not found"
	}
	delete(s.persons, name) // Delete the Person object from the map
	return &pb.DeletePersonResponse{Message: "Person deleted successfully"}, nil
}

// Server main function
func main() {
	persons := make(map[string]*pb.Person) // Create a new map to store the Person objects
	srv := &server{persons: persons}       // Create a new server instance with the map

	lis, err := net.Listen("tcp", ":50051") // Create a new listener on port 50051
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()                       // Create a new gRPC server instance
	pb.RegisterAddressBookServiceServer(s, srv) // Register the server with the gRPC server
	log.Printf("Server listening on %v", lis.Addr())
	if err := s.Serve(lis); err != nil { // Serve the gRPC server
		log.Fatalf("failed to serve: %v", err)
	}
}
